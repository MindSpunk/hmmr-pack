// Remove Old Recipes
recipes.remove(<chargers:wireless_charger>);
recipes.remove(<chargers:charger:2>);
recipes.remove(<chargers:charger:1>);
recipes.remove(<chargers:charger>);

// Register New Recipes
recipes.addShaped(<chargers:charger:2>, [[<ore:wireGtHexAluminium>, <ore:logWood>, <ore:wireGtHexAluminium>],[<ore:wireGtHexAluminium>, <gregtech:machine:504>, <ore:wireGtHexAluminium>], [<gregtech:meta_item_2:32213>, <ore:circuitExtreme>, <gregtech:meta_item_2:32213>]]);
recipes.addShaped(<chargers:charger:1>, [[<ore:wireGtHexGold>, <ore:logWood>, <ore:wireGtHexGold>],[<ore:wireGtHexGold>, <gregtech:machine:503>, <ore:wireGtHexGold>], [<gregtech:meta_item_1:32538>, <ore:circuitAdvanced>, <gregtech:meta_item_1:32538>]]);
recipes.addShaped(<chargers:charger>, [[<ore:wireGtHexCopper>, <ore:logWood>, <ore:wireGtHexCopper>],[<ore:wireGtHexCopper>, <gregtech:machine:502>, <ore:wireGtHexCopper>], [<gregtech:meta_item_1:32528>, <ore:circuitGood>, <gregtech:meta_item_1:32528>]]);