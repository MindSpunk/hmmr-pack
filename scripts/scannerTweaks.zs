// Remove Old
recipes.remove(<scannable:module_blank>);
recipes.remove(<scannable:scanner>);

// Add New
recipes.addShaped(<scannable:module_blank>, [[<ore:platePlastic>, <ore:circuitGood>, <ore:platePlastic>],[<ore:platePlastic>, <ore:circuitGood>, <ore:platePlastic>], [<ore:wireGtSingleGold>, <ore:wireGtSingleGold>, <ore:wireGtSingleGold>]]);
recipes.addShaped(<scannable:scanner>, [[<gregtech:meta_item_1:32681>, <gregtech:meta_item_1:32691>, <gregtech:meta_item_1:32681>],[<ore:platePlastic>, <ore:blockGlassColorless>, <ore:platePlastic>], [<gregtech:meta_item_1:32518>, <ore:circuitGood>, <gregtech:meta_item_1:32518>]]);