//
// Remove Unused Machines
//
mods.jei.JEI.removeAndHide(<cyberware:beacon>);
mods.jei.JEI.removeAndHide(<cyberware:beacon_large>);
mods.jei.JEI.removeAndHide(<cyberware:component_box>);
mods.jei.JEI.removeAndHide(<cyberware:blueprint_archive>);
mods.jei.JEI.removeAndHide(<cyberware:engineering_table>);
mods.jei.JEI.removeAndHide(<cyberware:radio_post>);
mods.jei.JEI.removeAndHide(<cyberware:scanner>);

//
// Get Greg Tech Machines
//
var assembler = mods.gregtech.recipe.RecipeMap.getByName("assembler");

//
// Greg Components
//
var carbon_plate = <gregtech:meta_item_2:32506>;
var carbon_mesh = <gregtech:meta_item_2:32505>;
var hv_motor = <gregtech:meta_item_1:32602>;
var hv_arm = <gregtech:meta_item_1:32652>;
var hv_piston = <gregtech:meta_item_1:32642>;
var hv_pump = <gregtech:meta_item_1:32612>;
var hv_emitter = <gregtech:meta_item_1:32682>;
var hv_sensor = <gregtech:meta_item_1:32692>;
var nano_processor = <gregtech:meta_item_2:32492>;
var good_electronic_circuit = <gregtech:meta_item_2:32489>;
var smd_resistor = <gregtech:meta_item_2:32459>;
var smd_diode = <gregtech:meta_item_2:32457>;
var smd_capacitor = <gregtech:meta_item_2:32458>;
var fine_silver_wire = <ore:wireFineSilver>;
var fine_titanium_wire = <ore:wireFineTitanium>;
var glass_fiber = <gregtech:meta_item_2:32453>;
var plastic_circuit_board = <gregtech:meta_item_2:32448>;
var greg_ram = <gregtech:meta_item_2:32485>;
var greg_nand = <gregtech:meta_item_2:32480>;
var small_lithium_battery = <gregtech:meta_item_1:32518>;
var medium_lithium_battery = <gregtech:meta_item_1:32528>;
var large_lithium_battery = <gregtech:meta_item_1:32538>;
var empty_cell = <gregtech:meta_item_1:32762>;
var diamond_lens = <gregtech:meta_item_1:15111>;

//
// Components
//
var actuator = <cyberware:component:0>;
var bioreactor = <cyberware:component:1>;
var titanium_mesh = <cyberware:component:2>;
var solid_state_circuitry = <cyberware:component:3>;
var chrome_plating = <cyberware:component:4>;
var fiber_optics = <cyberware:component:5>;
var fullerene_microstructures = <cyberware:component:6>;
var synthetic_nerves = <cyberware:component:7>;
var storage_cell = <cyberware:component:8>;
var microelectric_cells = <cyberware:component:9>;

//
// Machines
//
recipes.addShaped(<cyberware:surgery>, [
    [<ore:plateSteel>, <ore:plateGlass>, <ore:plateSteel>],
    [<gregtech:meta_item_2:32491>, <gregtech:machine:503>, <gregtech:meta_item_2:32491>],
    [<ore:plateSteel>, <gregtech:meta_item_2:32492>, <ore:plateSteel>]
]);

recipes.remove(<cyberware:surgery_chamber>);
recipes.addShaped(<cyberware:surgery_chamber>, [
    [<ore:plateSteel>, <ore:plateSteel>, <ore:plateSteel>],
    [<ore:plateSteel>, <minecraft:iron_door>, <ore:plateSteel>],
    [<gregtech:meta_item_1:32652>, <ore:toolHeadBuzzSawStainlessSteel>, <gregtech:meta_item_1:32652>]
]);

//
// Neuropozyne
//
var neuropozyne = <cyberware:neuropozyne>;
assembler.recipeBuilder()
    .inputs(<ore:pipeTinyPlastic>, <ore:plateCurvedTin>)
    .fluidInputs([<liquid:glycerol> * 500])
    .outputs(neuropozyne)
    .property("circuit", 10)
    .duration(60)
    .EUt(8)
    .buildAndRegister();

// Actuator
assembler.recipeBuilder()
    .inputs(hv_motor, carbon_plate)
    .outputs(actuator)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Bioreactor
assembler.recipeBuilder()
    .inputs(solid_state_circuitry, smd_capacitor * 4)
    .outputs(bioreactor)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Titanium Mesh
assembler.recipeBuilder()
    .inputs(fine_titanium_wire * 4)
    .outputs(titanium_mesh)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Solid State Circuitry
assembler.recipeBuilder()
    .inputs(good_electronic_circuit * 2, nano_processor * 1, plastic_circuit_board, smd_resistor, smd_diode)
    .outputs(solid_state_circuitry)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Chrome Plating
mods.jei.JEI.removeAndHide(chrome_plating);

// Fiber Optics
assembler.recipeBuilder()
    .inputs(glass_fiber * 4)
    .outputs(fiber_optics)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Fullerene Microstructures
assembler.recipeBuilder()
    .inputs(carbon_mesh * 2, fine_titanium_wire * 2)
    .outputs(fullerene_microstructures)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Synthetic Nerves
assembler.recipeBuilder()
    .inputs(fine_silver_wire * 4, nano_processor)
    .outputs(synthetic_nerves)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Storage Cell
assembler.recipeBuilder()
    .inputs(greg_nand * 64, greg_ram * 4, smd_resistor * 2, plastic_circuit_board)
    .outputs(storage_cell)
    .property("circuit", 10)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

// Microelectric Cells
mods.jei.JEI.removeAndHide(microelectric_cells);

//
// Augmentation Recipes
//
var aug_experience = <cyberware:exp_capsule>;
assembler.recipeBuilder()
    .inputs(storage_cell * 3, fullerene_microstructures)
    .outputs(aug_experience)
    .property("circuit", 27)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_arm_l = <cyberware:cyberlimbs:0>;
var aug_arm_r = <cyberware:cyberlimbs:1>;
var aug_leg_l = <cyberware:cyberlimbs:2>;
var aug_leg_r = <cyberware:cyberlimbs:3>;
assembler.recipeBuilder()
    .inputs(actuator, carbon_plate * 3, hv_arm)
    .outputs(aug_arm_l)
    .property("circuit", 11)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(actuator, carbon_plate * 3, hv_arm)
    .outputs(aug_arm_r)
    .property("circuit", 12)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(actuator, carbon_plate * 3, hv_piston, <ore:stickTitanium> * 2)
    .outputs(aug_leg_l)
    .property("circuit", 11)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(actuator, carbon_plate * 3, hv_piston, <ore:stickTitanium> * 2)
    .outputs(aug_leg_r)
    .property("circuit", 12)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_skin_solar = <cyberware:skin_upgrades:0>;
var aug_skin_spikes = <cyberware:skin_upgrades:1>;
var aug_skin_synth = <cyberware:skin_upgrades:2>;
var aug_skin_immuno = <cyberware:skin_upgrades:3>;
assembler.recipeBuilder()
    .inputs(<gregtech:meta_item_1:32750>, fullerene_microstructures * 2, <minecraft:leather>)
    .outputs(aug_skin_solar)
    .property("circuit", 13)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:stickTitanium> * 2, fullerene_microstructures * 2, <minecraft:leather>)
    .outputs(aug_skin_spikes)
    .property("circuit", 13)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(fullerene_microstructures * 2, <minecraft:leather> * 2)
    .outputs(aug_skin_synth)
    .property("circuit", 13)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:pipeTinyPlastic> * 2, fullerene_microstructures * 2, <minecraft:leather>)
    .outputs(aug_skin_immuno)
    .property("circuit", 13)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_foot_spur = <cyberware:foot_upgrades:0>;
var aug_foot_aqua = <cyberware:foot_upgrades:1>;
var aug_foot_wheel = <cyberware:foot_upgrades:2>;
assembler.recipeBuilder()
    .inputs(carbon_plate * 2, <ore:stickTitanium> * 2, <ore:stickSteel>, <ore:ringSteel> * 2)
    .outputs(aug_foot_spur)
    .property("circuit", 14)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(carbon_plate * 2, <ore:stickTitanium> * 2, actuator * 2, <ore:rotorSteel> * 2)
    .outputs(aug_foot_aqua)
    .property("circuit", 14)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(carbon_plate * 2, <ore:stickTitanium> * 2, <ore:ringRubber> * 2, actuator * 2)
    .outputs(aug_foot_wheel)
    .property("circuit", 14)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_leg_jump = <cyberware:leg_upgrades:0>;
var aug_leg_fall = <cyberware:leg_upgrades:1>;
assembler.recipeBuilder()
    .inputs(hv_piston * 2, carbon_plate * 2, <ore:plateSteel>)
    .outputs(aug_leg_jump)
    .property("circuit", 15)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(actuator * 2, carbon_plate * 1, <ore:plateSteel>)
    .outputs(aug_leg_fall)
    .property("circuit", 15)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_hand_hand = <cyberware:hand_upgrades:0>;
var aug_hand_claw = <cyberware:hand_upgrades:1>;
var aug_hand_fist = <cyberware:hand_upgrades:2>;
assembler.recipeBuilder()
    .inputs(titanium_mesh * 2, actuator * 4, <ore:stickSteel> * 2)
    .outputs(aug_hand_hand)
    .property("circuit", 16)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(titanium_mesh * 2, <ore:toolHeadSwordTitanium> * 2, actuator * 2)
    .outputs(aug_hand_claw)
    .property("circuit", 16)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(titanium_mesh * 2, <ore:plateTitanium> * 1)
    .outputs(aug_hand_fist)
    .property("circuit", 16)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_bone_lacing = <cyberware:bone_upgrades:0>;
var aug_bone_citrate = <cyberware:bone_upgrades:1>;
var aug_bone_battery = <cyberware:bone_upgrades:2>;
assembler.recipeBuilder()
    .inputs(titanium_mesh * 3, <ore:stickSteel> * 2)
    .outputs(aug_bone_lacing)
    .property("circuit", 17)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(carbon_plate * 1, <ore:stickTitanium> * 4)
    .outputs(aug_bone_citrate)
    .property("circuit", 17)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(titanium_mesh * 1, small_lithium_battery * 1)
    .outputs(aug_bone_battery)
    .property("circuit", 17)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_arm_quickdraw = <cyberware:arm_upgrades:0>;
assembler.recipeBuilder()
    .inputs(hv_motor * 1, <ore:gearSteel> * 2, <ore:ringSteel> * 2, titanium_mesh * 1)
    .outputs(aug_arm_quickdraw)
    .property("circuit", 18)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_muscle_reflex = <cyberware:muscle_upgrades:0>;
var aug_muscle_replace = <cyberware:muscle_upgrades:1>;
assembler.recipeBuilder()
    .inputs(solid_state_circuitry * 1, synthetic_nerves * 2)
    .outputs(aug_muscle_reflex)
    .property("circuit", 19)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(titanium_mesh * 3, actuator * 3)
    .outputs(aug_muscle_replace)
    .property("circuit", 19)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_lower_liver = <cyberware:lower_organs_upgrades:0>;
var aug_lower_meta = <cyberware:lower_organs_upgrades:1>;
var aug_lower_battery = <cyberware:lower_organs_upgrades:2>;
var aug_lower_adrenaline = <cyberware:lower_organs_upgrades:3>;
assembler.recipeBuilder()
    .inputs(carbon_mesh * 2, hv_pump, fullerene_microstructures)
    .outputs(aug_lower_liver)
    .property("circuit", 20)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:wireFineCopper> * 8, bioreactor, fullerene_microstructures)
    .outputs(aug_lower_meta)
    .property("circuit", 20)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(carbon_plate * 1, medium_lithium_battery)
    .outputs(aug_lower_battery)
    .property("circuit", 20)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:pipeTinyPlastic> * 4, hv_pump, fullerene_microstructures)
    .outputs(aug_lower_adrenaline)
    .property("circuit", 20)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_lungs_compressed = <cyberware:lungs_upgrades:0>;
var aug_lungs_hypo = <cyberware:lungs_upgrades:1>;
assembler.recipeBuilder()
    .inputs(empty_cell * 2, <ore:pipeTinyPlastic> * 2, fullerene_microstructures)
    .outputs(aug_lungs_compressed)
    .property("circuit", 21)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(empty_cell * 2, hv_pump, fullerene_microstructures)
    .outputs(aug_lungs_hypo)
    .property("circuit", 21)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_heart_defib = <cyberware:heart_upgrades:0>;
var aug_heart_platelet = <cyberware:heart_upgrades:1>;
var aug_heart_stem = <cyberware:heart_upgrades:2>;
var aug_heart_coupler = <cyberware:heart_upgrades:3>;
assembler.recipeBuilder()
    .inputs(<ore:wireFineGold> * 4, solid_state_circuitry, smd_capacitor * 16, fullerene_microstructures)
    .outputs(aug_heart_defib)
    .property("circuit", 22)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:pipeTinyPlastic> * 2, solid_state_circuitry, fullerene_microstructures)
    .outputs(aug_heart_platelet)
    .property("circuit", 22)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:pipeTinyPlastic> * 2, titanium_mesh * 1, bioreactor)
    .outputs(aug_heart_stem)
    .property("circuit", 22)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(bioreactor, <ore:wireFineGold> * 4, fullerene_microstructures)
    .outputs(aug_heart_coupler)
    .property("circuit", 22)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_dense_battery = <cyberware:dense_battery>;
assembler.recipeBuilder()
    .inputs(large_lithium_battery, fullerene_microstructures)
    .outputs(aug_dense_battery)
    .property("circuit", 23)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_heart = <cyberware:cyberheart>;
assembler.recipeBuilder()
    .inputs(hv_pump * 2, fullerene_microstructures * 2, <ore:pipeTinyPlastic> * 8)
    .outputs(aug_heart)
    .property("circuit", 24)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_brain_cortical = <cyberware:brain_upgrades:0>;
var aug_brain_ender = <cyberware:brain_upgrades:1>;
var aug_brain_transmitter = <cyberware:brain_upgrades:2>;
var aug_brain_neural = <cyberware:brain_upgrades:3>;
var aug_brain_threat = <cyberware:brain_upgrades:4>;
mods.jei.JEI.removeAndHide(<cyberware:brain_upgrades:5>);
assembler.recipeBuilder()
    .inputs(aug_experience, carbon_plate, synthetic_nerves)
    .outputs(aug_brain_cortical)
    .property("circuit", 25)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(<ore:enderpearl> * 1, <ore:stickTitanium> * 1, hv_emitter, solid_state_circuitry, synthetic_nerves)
    .outputs(aug_brain_ender)
    .property("circuit", 25)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(solid_state_circuitry, hv_emitter * 2, fullerene_microstructures, synthetic_nerves)
    .outputs(aug_brain_transmitter)
    .property("circuit", 25)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(synthetic_nerves * 2, solid_state_circuitry * 2, fullerene_microstructures)
    .outputs(aug_brain_neural)
    .property("circuit", 25)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(synthetic_nerves * 3, solid_state_circuitry, fullerene_microstructures)
    .outputs(aug_brain_threat)
    .property("circuit", 25)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_cybereyes = <cyberware:cybereyes>;
assembler.recipeBuilder()
    .inputs(diamond_lens * 2, hv_emitter, hv_sensor * 2, synthetic_nerves, fullerene_microstructures)
    .outputs(aug_cybereyes)
    .property("circuit", 26)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_cybereye_night_vis = <cyberware:cybereye_upgrades:0>;
var aug_cybereye_underwater_vis = <cyberware:cybereye_upgrades:1>;
var aug_cybereye_hudjack = <cyberware:cybereye_upgrades:2>;
var aug_cybereye_targetting = <cyberware:cybereye_upgrades:3>;
var aug_cybereye_distance = <cyberware:cybereye_upgrades:4>;
assembler.recipeBuilder()
    .inputs(hv_sensor * 2, hv_emitter * 1, solid_state_circuitry, synthetic_nerves)
    .outputs(aug_cybereye_night_vis)
    .property("circuit", 28)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(diamond_lens * 2, hv_motor, solid_state_circuitry, synthetic_nerves)
    .outputs(aug_cybereye_underwater_vis)
    .property("circuit", 28)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(solid_state_circuitry, synthetic_nerves * 2)
    .outputs(aug_cybereye_hudjack)
    .property("circuit", 28)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(synthetic_nerves * 2, solid_state_circuitry * 2)
    .outputs(aug_cybereye_targetting)
    .property("circuit", 28)
    .duration(300)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(solid_state_circuitry * 2, synthetic_nerves)
    .outputs(aug_cybereye_distance)
    .property("circuit", 28)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var aug_eye_hudlens = <cyberware:eye_upgrades>;
assembler.recipeBuilder()
    .inputs(diamond_lens * 2, synthetic_nerves, solid_state_circuitry)
    .outputs(aug_eye_hudlens)
    .property("circuit", 29)
    .duration(300)
    .EUt(16)
    .buildAndRegister();

var quantum_processor = <gregtech:meta_item_2:32494>;
var ev_emitter = <gregtech:meta_item_1:32683>;
var ev_sensor = <gregtech:meta_item_1:32693>;
var mv_field_generator = <gregtech:meta_item_1:32671>;
var dense_titanium_plate = <ore:plateDenseTitanium>;

var aug_re_skin_cloak = <rewired:skin:0>;
var aug_re_skin_aegis = <rewired:skin:1>;
assembler.recipeBuilder()
    .inputs(quantum_processor, diamond_lens, titanium_mesh * 6)
    .outputs(aug_re_skin_cloak)
    .property("circuit", 30)
    .duration(500)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(ev_emitter, titanium_mesh * 4, carbon_mesh * 8)
    .outputs(aug_re_skin_aegis)
    .property("circuit", 30)
    .duration(500)
    .EUt(16)
    .buildAndRegister();

var aug_re_torso_derps = <rewired:torso:0>;
var aug_re_torso_stomach = <rewired:torso:1>;
assembler.recipeBuilder()
    .inputs(ev_sensor, mv_field_generator, titanium_mesh * 4)
    .outputs(aug_re_torso_derps)
    .property("circuit", 31)
    .duration(500)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(bioreactor * 4, nano_processor * 1, titanium_mesh * 4, carbon_mesh * 2)
    .outputs(aug_re_torso_stomach)
    .property("circuit", 31)
    .duration(500)
    .EUt(16)
    .buildAndRegister();

var aug_re_cranium_interface = <rewired:cranium:1>;
assembler.recipeBuilder()
    .inputs(synthetic_nerves * 6, nano_processor)
    .outputs(aug_re_cranium_interface)
    .property("circuit", 32)
    .duration(500)
    .EUt(16)
    .buildAndRegister();

var aug_re_hand_fist = <rewired:hand:0>;
var aug_re_hand_barrier = <rewired:hand:1>;
assembler.recipeBuilder()
    .inputs(dense_titanium_plate * 4, carbon_plate * 2)
    .outputs(aug_re_hand_fist)
    .property("circuit", 33)
    .duration(500)
    .EUt(16)
    .buildAndRegister();
assembler.recipeBuilder()
    .inputs(mv_field_generator * 2, ev_sensor, nano_processor)
    .outputs(aug_re_hand_barrier)
    .property("circuit", 33)
    .duration(500)
    .EUt(16)
    .buildAndRegister();

mods.jei.JEI.removeAndHide(<rewired:skin:2>);
mods.jei.JEI.removeAndHide(<rewired:cranium:0>);
mods.jei.JEI.removeAndHide(<rewired:foot:0>);
