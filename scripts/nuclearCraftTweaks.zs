var fluid_canner = mods.gregtech.recipe.RecipeMap.getByName("fluid_canner");
var chemical_bath = mods.gregtech.recipe.RecipeMap.getByName("chemical_bath");

var empty_cell = <gregtech:meta_item_1:32762>;
var bioplastic = <nuclearcraft:part:6>;
var radaway = <nuclearcraft:radaway>;
var radaway_slow = <nuclearcraft:radaway_slow>;

## Replace Bioplastic with Polyethylene Sheet
recipes.replaceAllOccurences(bioplastic, <gregtech:meta_item_1:12141>);
recipes.replaceAllOccurences(<ore:ingotAluminum>, <ore:ingotAluminium>);

## Remove old radaway recipes
chemical_bath.findRecipe(8, [bioplastic * 2], [<liquid:radaway> * 250]).remove();
chemical_bath.findRecipe(8, [bioplastic * 2], [<liquid:radaway_slow> * 250]).remove();
chemical_bath.findRecipe(8, [radaway], [<liquid:redstone> * 200]).remove();

## Remove Bioplastic
mods.jei.JEI.removeAndHide(bioplastic);

## Remove RF Power Storage
mods.jei.JEI.removeAndHide(<nuclearcraft:voltaic_pile_basic>);
mods.jei.JEI.removeAndHide(<nuclearcraft:voltaic_pile_advanced>);
mods.jei.JEI.removeAndHide(<nuclearcraft:voltaic_pile_du>);
mods.jei.JEI.removeAndHide(<nuclearcraft:voltaic_pile_elite>);

## Remove RF Power Storage
mods.jei.JEI.removeAndHide(<nuclearcraft:lithium_ion_battery_basic>);
mods.jei.JEI.removeAndHide(<nuclearcraft:lithium_ion_battery_advanced>);
mods.jei.JEI.removeAndHide(<nuclearcraft:lithium_ion_battery_du>);
mods.jei.JEI.removeAndHide(<nuclearcraft:lithium_ion_battery_elite>);

## Remove RF Power Storage
mods.jei.JEI.removeAndHide(<nuclearcraft:lithium_ion_cell>);

## Remove OP Items
mods.jei.JEI.removeAndHide(<nuclearcraft:portable_ender_chest>);
mods.jei.JEI.removeAndHide(<nuclearcraft:dominos>);
mods.jei.JEI.removeAndHide(<nuclearcraft:smore>);
mods.jei.JEI.removeAndHide(<nuclearcraft:moresmore>);
mods.jei.JEI.removeAndHide(<nuclearcraft:foursmore>);

## Alternative Radaway Recipe
fluid_canner.recipeBuilder()
    .inputs(empty_cell)
    .fluidInputs([<liquid:radaway> * 250])
    .outputs(radaway * 1)
    .duration(100)
    .EUt(30)
    .buildAndRegister();

## Alternative Slow Radaway Recipe
fluid_canner.recipeBuilder()
    .inputs(empty_cell)
    .fluidInputs([<liquid:radaway_slow> * 250])
    .outputs(radaway_slow * 1)
    .duration(100)
    .EUt(30)
    .buildAndRegister();
