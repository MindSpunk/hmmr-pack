var centrifuge = mods.gregtech.recipe.RecipeMap.getByName("centrifuge");
var assembler = mods.gregtech.recipe.RecipeMap.getByName("assembler");
var chemical_reactor = mods.gregtech.recipe.RecipeMap.getByName("chemical_reactor");
var chemical_bath = mods.gregtech.recipe.RecipeMap.getByName("chemical_bath");
var autoclave = mods.gregtech.recipe.RecipeMap.getByName("autoclave");
var alloy_smelter = mods.gregtech.recipe.RecipeMap.getByName("alloy_smelter");

// Fluids
var fluid_borax = <nuclearcraft:fluid_borax_solution>;

// Electric Motors
var lv_motor = <gregtech:meta_item_1:32600>;
var mv_motor = <gregtech:meta_item_1:32601>;
var hv_motor = <gregtech:meta_item_1:32602>;
var ev_motor = <gregtech:meta_item_1:32603>;
var iv_motor = <gregtech:meta_item_1:32604>;
var luv_motor = <gregtech:meta_item_1:32605>;
var zpm_motor = <gregtech:meta_item_1:32606>;
var uv_motor = <gregtech:meta_item_1:32607>;

// Electric Pumps
var lv_pump = <gregtech:meta_item_1:32610>;
var mv_pump = <gregtech:meta_item_1:32611>;
var hv_pump = <gregtech:meta_item_1:32612>;
var ev_pump = <gregtech:meta_item_1:32613>;
var iv_pump = <gregtech:meta_item_1:32614>;
var luv_pump = <gregtech:meta_item_1:32615>;
var zpm_pump = <gregtech:meta_item_1:32616>;
var uv_pump = <gregtech:meta_item_1:32617>;

// Electric Conveyors
var lv_conveyor = <gregtech:meta_item_1:32630>;
var mv_conveyor = <gregtech:meta_item_1:32631>;
var hv_conveyor = <gregtech:meta_item_1:32632>;
var ev_conveyor = <gregtech:meta_item_1:32633>;
var iv_conveyor = <gregtech:meta_item_1:32634>;
var luv_conveyor = <gregtech:meta_item_1:32635>;
var zpm_conveyor = <gregtech:meta_item_1:32636>;
var uv_conveyor = <gregtech:meta_item_1:32637>;

// Electric Pistons
var lv_piston = <gregtech:meta_item_1:32640>;
var mv_piston = <gregtech:meta_item_1:32641>;
var hv_piston = <gregtech:meta_item_1:32642>;
var ev_piston = <gregtech:meta_item_1:32643>;
var iv_piston = <gregtech:meta_item_1:32644>;
var luv_piston = <gregtech:meta_item_1:32645>;
var zpm_piston = <gregtech:meta_item_1:32646>;
var uv_piston = <gregtech:meta_item_1:32647>;

// Electric Arms
var lv_arm = <gregtech:meta_item_1:32650>;
var mv_arm = <gregtech:meta_item_1:32651>;
var hv_arm = <gregtech:meta_item_1:32652>;
var ev_arm = <gregtech:meta_item_1:32653>;
var iv_arm = <gregtech:meta_item_1:32654>;
var luv_arm = <gregtech:meta_item_1:32655>;
var zpm_arm = <gregtech:meta_item_1:32656>;
var uv_arm = <gregtech:meta_item_1:32657>;

// Electric Field Generators
var lv_field_generator = <gregtech:meta_item_1:32670>;
var mv_field_generator = <gregtech:meta_item_1:32671>;
var hv_field_generator = <gregtech:meta_item_1:32672>;
var ev_field_generator = <gregtech:meta_item_1:32673>;
var iv_field_generator = <gregtech:meta_item_1:32674>;
var luv_field_generator = <gregtech:meta_item_1:32675>;
var zpm_field_generator = <gregtech:meta_item_1:32676>;
var uv_field_generator = <gregtech:meta_item_1:32677>;

// Electric Emitter
var lv_emitter = <gregtech:meta_item_1:32680>;
var mv_emitter = <gregtech:meta_item_1:32681>;
var hv_emitter = <gregtech:meta_item_1:32682>;
var ev_emitter = <gregtech:meta_item_1:32683>;
var iv_emitter = <gregtech:meta_item_1:32684>;
var luv_emitter = <gregtech:meta_item_1:32685>;
var zpm_emitter = <gregtech:meta_item_1:32686>;
var uv_emitter = <gregtech:meta_item_1:32687>;

// Electric Sensor
var lv_sensor = <gregtech:meta_item_1:32690>;
var mv_sensor = <gregtech:meta_item_1:32691>;
var hv_sensor = <gregtech:meta_item_1:32692>;
var ev_sensor = <gregtech:meta_item_1:32693>;
var iv_sensor = <gregtech:meta_item_1:32694>;
var luv_sensor = <gregtech:meta_item_1:32695>;
var zpm_sensor = <gregtech:meta_item_1:32696>;
var uv_sensor = <gregtech:meta_item_1:32697>;

// Gregtech Circuits
var circuit_primitive = <ore:circuitPrimitive>;
var circuit_basic = <ore:circuitBasic>;
var circuit_good = <ore:circuitGood>;
var circuit_advanced = <ore:circuitAdvanced>;
var circuit_extreme = <ore:circuitExtreme>;
var circuit_elite = <ore:circuitElite>;
var circuit_master = <ore:circuitMaster>;
var circuit_ultimate = <ore:circuitUltimate>;
var circuit_superconductor = <ore:circuitSuperconductor>;
var circuit_infinite = <ore:circuitInfinite>;

// Gregtech Batteries
var small_acid_battery = <gregtech:meta_item_1:32510>;
var small_mercury_battery = <gregtech:meta_item_1:32511>;
var small_cadmium_battery = <gregtech:meta_item_1:32517>;
var small_lithium_battery = <gregtech:meta_item_1:32518>;
var small_sodium_battery = <gregtech:meta_item_1:32519>;
var medium_acid_battery = <gregtech:meta_item_1:32520>;
var medium_mercury_battery = <gregtech:meta_item_1:32521>;
var medium_cadmium_battery = <gregtech:meta_item_1:32527>;
var medium_lithium_battery = <gregtech:meta_item_1:32528>;
var medium_sodium_battery = <gregtech:meta_item_1:32529>;
var large_acid_battery = <gregtech:meta_item_1:32530>;
var large_mercury_battery = <gregtech:meta_item_1:32531>;
var large_cadmium_battery = <gregtech:meta_item_1:32537>;
var large_lithium_battery = <gregtech:meta_item_1:32538>;
var large_sodium_battery = <gregtech:meta_item_1:32539>;

// Gregtech Lenses
var glass_lens = <gregtech:meta_item_1:15209>;
var diamond_lens = <gregtech:meta_item_1:15111>;

// Gregtech Circuit Boards
var coated_circuit_board = <gregtech:meta_item_2:32443>;
var epoxy_circuit_board = <gregtech:meta_item_2:32444>;
var fibre_reinforced_circuit_board = <gregtech:meta_item_2:32445>;
var ml_fibre_reinforced_circuit_board = <gregtech:meta_item_2:32446>;
var phenolic_circuit_board = <gregtech:meta_item_2:32447>;
var plastic_circuit_board = <gregtech:meta_item_2:32448>;
var wetware_lifesupport_circuit_board = <gregtech:meta_item_2:32449>;

// Plates
var plastic_plate = <ore:platePlastic>;
var titanium_plate = <ore:plateTitanium>;
var dense_titanium_plate = <ore:plateDenseTitanium>;

//
// ADVANCED ROCKETRY ITEMS
//

var hover_upgrade = <advancedrocketry:itemupgrade:0>;               // HAS RECIPE
var flight_speed_upgrade = <advancedrocketry:itemupgrade:1>;        // HAS RECIPE
var bionic_leg = <advancedrocketry:itemupgrade:2>;                  // HAS RECIPE
var padded_boots = <advancedrocketry:itemupgrade:3>;                // HAS RECIPE
var antifog_visor = <advancedrocketry:itemupgrade:4>;               // HAS RECIPE
var elevator_chip = <advancedrocketry:elevatorchip>;                // HAS RECIPE
var satellite_chip = <advancedrocketry:satelliteidchip>;            // HAS RECIPE
var planet_chip = <advancedrocketry:planetidchip>;                  // HAS RECIPE
var space_station_chip = <advancedrocketry:spacestationchip>;       // HAS RECIPE
var asteroid_chip = <advancedrocketry:asteroidchip>;                // HAS RECIPE
var tracking_circuit = <advancedrocketry:ic:1>;                     // HAS RECIPE
var data_unit = <advancedrocketry:dataunit>;                        // HAS RECIPE  
var beacon_finder = <advancedrocketry:beaconfinder>;                // HAS RECIPE
var atmos_analyzer = <advancedrocketry:atmanalyser>;                // HAS RECIPE
var biome_changer_remote = <advancedrocketry:biomechanger>;         // HAS RECIPE
var biome_changer = <advancedrocketry:satelliteprimaryfunction:5>;  // HAS RECIPE

var rocket_builder = <advancedrocketry:rocketbuilder>;              // HAS RECIPE
var rocket_motor = <advancedrocketry:rocketmotor>;                  // HAS RECIPE
var advanced_rocket_motor = <advancedrocketry:advrocketmotor>;      // HAS RECIPE
var liquid_fuel_tank = <advancedrocketry:fueltank>;                 // HAS RECIPE
var fueling_station = <advancedrocketry:fuelingstation>;            // HAS RECIPE
var monitoring_station = <advancedrocketry:monitoringstation>;      // HAS RECIPE
var satellite_builder = <advancedrocketry:satellitebuilder>;        // HAS RECIPE
var data_bus = <advancedrocketry:loader>;                           // HAS RECIPE
var satellite_bay = <advancedrocketry:loader:1>;                    // HAS RECIPE
var guidance_computer_access_hatch = <advancedrocketry:loader:6>;   // HAS RECIPE
var observatory = <advancedrocketry:observatory>;                   // HAS RECIPE
var guidance_computer = <advancedrocketry:guidancecomputer>;        // HAS RECIPE
var astrobody_data_analyser = <advancedrocketry:planetanalyser>;    // HAS RECIPE
var satellite_terminal = <advancedrocketry:satellitecontrolcenter>; // HAS RECIPE
var space_station_builder = <advancedrocketry:stationbuilder>;      // HAS RECIPE

// Remove Old Recipes
recipes.remove(rocket_builder);
recipes.remove(rocket_motor);
recipes.remove(advanced_rocket_motor);
recipes.remove(liquid_fuel_tank);
recipes.remove(fueling_station);
recipes.remove(monitoring_station);
recipes.remove(satellite_builder);
recipes.remove(data_bus);
recipes.remove(satellite_bay);
recipes.remove(guidance_computer_access_hatch);
recipes.remove(observatory);
recipes.remove(guidance_computer);
recipes.remove(astrobody_data_analyser);
recipes.remove(satellite_terminal);
recipes.remove(space_station_builder);
recipes.remove(<advancedrocketry:oxygenscrubber>);
recipes.remove(<advancedrocketry:oxygencharger>);
recipes.remove(<advancedrocketry:landingpad>);
recipes.remove(<advancedrocketry:oxygenvent>);
recipes.remove(<advancedrocketry:warpcore>);
recipes.remove(<advancedrocketry:warpmonitor>);
recipes.remove(<advancedrocketry:orientationcontroller>);
recipes.remove(<advancedrocketry:oxygendetection>);
recipes.remove(<advancedrocketry:terraformer>);
recipes.remove(<advancedrocketry:biomescanner>);
recipes.remove(<advancedrocketry:suitworkstation>);
recipes.remove(<advancedrocketry:solarpanel>);
recipes.remove(<advancedrocketry:microwavereciever>);
recipes.remove(<advancedrocketry:drill>);
recipes.remove(<advancedrocketry:gravitycontroller>);
recipes.remove(<advancedrocketry:deployablerocketbuilder>);

recipes.remove(elevator_chip);
recipes.remove(satellite_chip);
recipes.remove(planet_chip);
recipes.remove(tracking_circuit);
recipes.remove(asteroid_chip);
recipes.remove(space_station_chip);

// Elevator Chip
recipes.addShapeless(elevator_chip, [circuit_basic, lv_sensor]);
recipes.addShapeless(elevator_chip, [space_station_chip]);

// Satellite Chip
recipes.addShapeless(satellite_chip, [elevator_chip]);

// Planet Chip
recipes.addShapeless(planet_chip, [satellite_chip]);

// Tracking Chip
recipes.addShapeless(tracking_circuit, [planet_chip]);

// Asteroid Chip
recipes.addShapeless(asteroid_chip, [tracking_circuit]);

// Statipon Chip
recipes.addShapeless(space_station_chip, [asteroid_chip]);

// Data Unit
recipes.remove(data_unit);
recipes.addShapeless(data_unit, [circuit_basic, lv_emitter]);


// Hover Upgrade
assembler.recipeBuilder()
    .inputs(circuit_advanced, plastic_circuit_board)
    .outputs(hover_upgrade)
    .property("circuit", 40)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Flight Speed Upgrade
assembler.recipeBuilder()
    .inputs(circuit_good * 2, plastic_circuit_board)
    .outputs(flight_speed_upgrade)
    .property("circuit", 41)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Bionic Leg
assembler.recipeBuilder()
    .inputs(mv_motor, mv_piston, circuit_good, plastic_circuit_board)
    .outputs(bionic_leg)
    .property("circuit", 42)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Padded Boots
assembler.recipeBuilder()
    .inputs(<minecraft:leather_boots>, small_lithium_battery, circuit_good, plastic_circuit_board, mv_piston * 2)
    .outputs(padded_boots)
    .property("circuit", 43)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// AntiFog Visor
assembler.recipeBuilder()
    .inputs(glass_lens * 2, small_lithium_battery, circuit_good, plastic_circuit_board, plastic_plate * 2)
    .outputs(antifog_visor)
    .property("circuit", 44)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Atmosphere Analyzer
assembler.recipeBuilder()
    .inputs(diamond_lens, small_lithium_battery, circuit_good, plastic_circuit_board, plastic_plate * 2)
    .outputs(atmos_analyzer)
    .property("circuit", 45)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Biome Changer Remote
assembler.recipeBuilder()
    .inputs(mv_emitter, mv_sensor, small_lithium_battery, circuit_good, plastic_circuit_board, plastic_plate * 2)
    .outputs(biome_changer_remote)
    .property("circuit", 46)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Biome Changer
assembler.recipeBuilder()
    .inputs(hv_emitter, hv_sensor, circuit_advanced, plastic_circuit_board, plastic_plate * 2)
    .outputs(biome_changer)
    .property("circuit", 47)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Beacon Finder
assembler.recipeBuilder()
    .inputs(antifog_visor, tracking_circuit)
    .outputs(biome_changer_remote)
    .property("circuit", 48)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Rocket Motor
assembler.recipeBuilder()
    .inputs(<ore:ingotSteel> * 3, titanium_plate * 3)
    .outputs(rocket_motor)
    .property("circuit", 49)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Advanced Rocket Motor
assembler.recipeBuilder()
    .inputs(<ore:ingotTitanium> * 3, dense_titanium_plate * 3)
    .outputs(advanced_rocket_motor)
    .property("circuit", 50)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Liquid Fuel Tank
assembler.recipeBuilder()
    .inputs(<ore:plateSteel> * 4, <ore:stickSteel> * 4)
    .outputs(liquid_fuel_tank)
    .property("circuit", 51)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Space Suit Helmet
recipes.remove(<advancedrocketry:spacehelmet>);
assembler.recipeBuilder()
    .inputs(<ore:platePlastic> * 4, <ore:blockWool> * 4, <ore:plateGlass> * 1)
    .outputs(<advancedrocketry:spacehelmet>)
    .property("circuit", 52)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Space Suit Chest
recipes.remove(<advancedrocketry:spacechestplate>);
assembler.recipeBuilder()
    .inputs(<ore:platePlastic> * 4, <ore:blockWool> * 4, <ore:fanSteel> * 1)
    .outputs(<advancedrocketry:spacechestplate>)
    .property("circuit", 53)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Space Suit Legs
recipes.remove(<advancedrocketry:spaceleggings>);
assembler.recipeBuilder()
    .inputs(<ore:platePlastic> * 2, <ore:blockWool> * 4)
    .outputs(<advancedrocketry:spaceleggings>)
    .property("circuit", 54)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Space Suit Boots
recipes.remove(<advancedrocketry:spaceboots>);
assembler.recipeBuilder()
    .inputs(<ore:platePlastic> * 2, <ore:blockWool> * 2, <ore:stickIron> * 1)
    .outputs(<advancedrocketry:spaceboots>)
    .property("circuit", 55)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Ore Scanner
recipes.remove(<advancedrocketry:orescanner>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 4, <gregtech:meta_item_1:32518>, <ore:plateGlass>, mv_sensor, mv_emitter)
    .outputs(<advancedrocketry:orescanner>)
    .property("circuit", 60)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Composition Scanner
recipes.remove(<advancedrocketry:satelliteprimaryfunction:1>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 4, <ore:circuitGood>, <ore:plateGlass>, mv_sensor, mv_emitter)
    .outputs(<advancedrocketry:satelliteprimaryfunction:1>)
    .property("circuit", 61)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Mass Detector
recipes.remove(<advancedrocketry:satelliteprimaryfunction:2>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 4, <ore:circuitGood>, <ore:gemDilithium>, mv_sensor, mv_emitter)
    .outputs(<advancedrocketry:satelliteprimaryfunction:2>)
    .property("circuit", 62)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Microwave Transmitter
recipes.remove(<advancedrocketry:satelliteprimaryfunction:3>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 4, <ore:circuitGood>, <advancedrocketry:ic:1>, mv_emitter * 2)
    .outputs(<advancedrocketry:satelliteprimaryfunction:3>)
    .property("circuit", 63)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Ore Mapper
recipes.remove(<advancedrocketry:satelliteprimaryfunction:4>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 4, <advancedrocketry:orescanner>, <ore:stickCopper>)
    .outputs(<advancedrocketry:satelliteprimaryfunction:4>)
    .property("circuit", 64)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Satellite
recipes.remove(<advancedrocketry:satellite>);
assembler.recipeBuilder()
    .inputs(<ore:plateAluminium> * 6, <ore:plateTitanium> * 2, <gregtech:meta_item_2:32448>)
    .outputs(<advancedrocketry:satellite>)
    .property("circuit", 65)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// CO2 Scrubber Cartridge
recipes.remove(<advancedrocketry:carbonscrubbercartridge>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 6, <minecraft:iron_bars> * 3)
    .outputs(<advancedrocketry:carbonscrubbercartridge>)
    .property("circuit", 66)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Satellite Solar Panel
recipes.remove(<advancedrocketry:satellitepowersource>);
assembler.recipeBuilder()
    .inputs(<gregtech:meta_item_1:32750>, <ore:plateIron>)
    .outputs(<advancedrocketry:satellitepowersource>)
    .property("circuit", 67)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Satellite Solar Panel
recipes.remove(<advancedrocketry:satellitepowersource:1>);
assembler.recipeBuilder()
    .inputs(<advancedrocketry:satellitepowersource> * 6, <ore:plateIron> * 3)
    .outputs(<advancedrocketry:satellitepowersource:1>)
    .property("circuit", 67)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Low Pressure Tank
recipes.remove(<advancedrocketry:pressuretank:0>);
assembler.recipeBuilder()
    .inputs(<ore:plateIron> * 4)
    .outputs(<advancedrocketry:pressuretank:0>)
    .property("circuit", 68)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Pressure Tank
recipes.remove(<advancedrocketry:pressuretank:1>);
assembler.recipeBuilder()
    .inputs(<ore:plateSteel> * 4)
    .outputs(<advancedrocketry:pressuretank:1>)
    .property("circuit", 68)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// High Pressure Tank
recipes.remove(<advancedrocketry:pressuretank:2>);
assembler.recipeBuilder()
    .inputs(<ore:plateAluminium> * 4)
    .outputs(<advancedrocketry:pressuretank:2>)
    .property("circuit", 68)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Super High Pressure Tank
recipes.remove(<advancedrocketry:pressuretank:3>);
assembler.recipeBuilder()
    .inputs(<ore:plateTitanium> * 4)
    .outputs(<advancedrocketry:pressuretank:3>)
    .property("circuit", 68)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Jetpack
recipes.remove(<advancedrocketry:jetpack>);
assembler.recipeBuilder()
    .inputs(<advancedrocketry:pressuretank:*> * 2, <ore:plateSteel> * 2, <ore:plateTitanium> * 2, mv_pump)
    .outputs(<advancedrocketry:jetpack>)
    .property("circuit", 69)
    .duration(400)
    .EUt(16)
    .buildAndRegister();

// Carbon Scrubber Cleaning Early
centrifuge.recipeBuilder()
    .inputs(<advancedrocketry:carbonscrubbercartridge:32766> * 1)
    .outputs(<advancedrocketry:carbonscrubbercartridge:11468> * 1, <minecraft:coal:1> * 1)
    .duration(120)
    .EUt(16)
    .buildAndRegister();

// Carbon Scrubber Cleaning Efficient
chemical_bath.recipeBuilder()
    .inputs(<advancedrocketry:carbonscrubbercartridge:32766> * 1)
    .fluidInputs([<liquid:water> * 1000])
    .outputs(<advancedrocketry:carbonscrubbercartridge> * 1, <minecraft:coal:1> * 2)
    .duration(80)
    .EUt(16)
    .buildAndRegister();

// Dilithium growth
autoclave.recipeBuilder()
    .inputs(<libvulpes:productdust> * 1)
    .fluidInputs([<liquid:hydrogen_sulfide> * 100])
    .outputs(<libvulpes:productgem> * 1)
    .duration(100)
    .EUt(32)
    .buildAndRegister();

var basic_machine_core = <libvulpes:structuremachine>;
var advanced_machine_core = <libvulpes:advstructuremachine>;

recipes.remove(<advancedrocketry:blocklens>);
recipes.addShaped(<advancedrocketry:blocklens>, [
    [null, null, null],
    [<ore:lensGlass>, <ore:blockGlassColorless>, <ore:lensGlass>],
    [null, null, null]
]);

recipes.remove(<advancedrocketry:spacelaser>);
recipes.addShaped(<advancedrocketry:spacelaser>, [
    [<ore:circuitAdvanced>, <advancedrocketry:ic:1>, <ore:circuitAdvanced>],
    [<ore:plateTitanium>, <libvulpes:structuremachine>, <ore:plateTitanium>],
    [<ore:fanSteel>, <ore:lensDiamond>, <ore:fanSteel>]
]);

recipes.remove(<advancedrocketry:gravitymachine>);
recipes.addShaped(<advancedrocketry:gravitymachine>, [
    [null, null, null],
    [<ore:plateTitanium>, <advancedrocketry:satelliteprimaryfunction:2>, <ore:plateTitanium>],
    [<ore:plateTitanium>, <advancedrocketry:warpcore>, <ore:plateTitanium>]
]);

recipes.remove(<advancedrocketry:beacon>);
recipes.addShaped(<advancedrocketry:beacon>, [
    [<gregtech:meta_item_1:12033>, <gregtech:cable:4018>, <gregtech:meta_item_1:12033>],
    [<gregtech:meta_item_2:32489>, <libvulpes:structuremachine>, <advancedrocketry:ic:1>],
    [<gregtech:meta_item_1:12033>, <gregtech:cable:4018>, <gregtech:meta_item_1:12033>]
]);

recipes.remove(<advancedrocketry:spaceelevatorcontroller>);
recipes.addShaped(<advancedrocketry:spaceelevatorcontroller>, [
    [null, <ore:circuitGood>, null],
    [mv_motor, <libvulpes:structuremachine>, mv_motor],
    [<ore:wireGtHexCopper>, <ore:wireGtHexCopper>, <ore:wireGtHexCopper>]
]);

recipes.remove(<advancedrocketry:forcefieldprojector>);
recipes.addShaped(<advancedrocketry:forcefieldprojector>, [
    [<ore:plateAluminium>, mv_field_generator, <ore:plateAluminium>],
    [<ore:plateAluminium>, <ore:gemDilithium>, <ore:plateAluminium>],
    [<ore:plateAluminium>, <libvulpes:structuremachine>, <ore:plateAluminium>]
]);

recipes.remove(<advancedrocketry:railgun>);
recipes.addShaped(<advancedrocketry:railgun>, [
    [null, <advancedrocketry:ic:1>, null],
    [<ore:circuitGood>, <libvulpes:structuremachine>, <ore:circuitGood>],
    [<ore:fanSteel>, <ore:wireGtHexCopper>, <ore:fanSteel>]
]);

recipes.remove(<advancedrocketry:altitudecontroller>);
recipes.addShapeless(<advancedrocketry:altitudecontroller>, [<ore:plateGlass>,<libvulpes:structuremachine>,<ore:circuitBasic>]);

recipes.remove(<advancedrocketry:stationmarker>);
recipes.addShapeless(<advancedrocketry:stationmarker>, [<advancedrocketry:ic:1>,<advancedrocketry:loader:1>,<gregtech:meta_item_1:32518>]);

recipes.remove(<advancedrocketry:solargenerator>);
recipes.addShapeless(<advancedrocketry:solargenerator>, [<libvulpes:forgepoweroutput>,<advancedrocketry:solarpanel>,<gregtech:meta_item_1:32518>]);

recipes.remove(<advancedrocketry:circlelight>);
recipes.addShapeless(<advancedrocketry:circlelight>, [<ore:plateIron>,<ore:blockGlowstone>]);

// Rocket Builder
recipes.addShaped(rocket_builder, [
    [<ore:stickTitanium>, <ore:plateGlass>, <ore:stickTitanium>],
    [<ore:circuitGood>, basic_machine_core, <ore:circuitGood>], 
    [<ore:gearTitanium>, <ore:plateIron>, <ore:gearTitanium>]
    ]);

// Fueling Station
recipes.addShaped(fueling_station, [
    [<ore:plateIron>, <ore:plateGlass>, <ore:plateIron>],
    [<ore:circuitBasic>, basic_machine_core, <ore:fanSteel>], 
    [<ore:plateTin>, <ore:plateTin>, <ore:plateTin>]
    ]);

// Monitoring Station
recipes.addShaped(monitoring_station, [
    [<ore:stickCopper>, mv_sensor, <ore:stickCopper>],
    [<ore:stickCopper>, basic_machine_core, <ore:stickCopper>],
    [<ore:stickCopper>, small_lithium_battery, <ore:stickCopper>]
    ]);

// Satellite Builder
recipes.addShaped(satellite_builder, [
    [mv_arm, mv_conveyor, mv_motor],
    [<ore:circuitGood>, basic_machine_core, <ore:circuitAdvanced>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
    ]);

// Data Bus
recipes.addShapeless(data_bus, [basic_machine_core,data_unit]);

// Forge Power Input
recipes.removeShaped(<libvulpes:forgepowerinput>, [[null, <libvulpes:battery>, null],[<libvulpes:battery>, <libvulpes:structuremachine>, <libvulpes:battery>], [null, <libvulpes:battery>, null]]);
recipes.addShapeless(<libvulpes:forgepowerinput>, [basic_machine_core, small_lithium_battery]);

// Data Bus
recipes.remove(<libvulpes:gtplug>);
recipes.addShapeless(<libvulpes:gtplug>, [basic_machine_core,<ore:plateTitanium>,small_lithium_battery]);

// Fluid Pump
recipes.remove(<advancedrocketry:blockpump>);
recipes.addShaped(<advancedrocketry:blockpump>, [
    [<ore:plateIron>, <ore:plateGlass>, <ore:plateIron>],
    [<gregtech:meta_item_1:32611>, <libvulpes:structuremachine>, <gregtech:meta_item_1:32611>],
    [<ore:plateIron>, <ore:circuitBasic>, <ore:plateIron>]
]);

// Black Hole Generator
recipes.addShaped(<advancedrocketry:blackholegenerator>, [
    [<ore:plateIron>, <ore:plateGlass>, <ore:plateIron>],
    [<gregtech:meta_item_1:32538>, <libvulpes:advstructuremachine>, <gregtech:meta_item_1:32538>],
    [<ore:plateIron>, <ore:circuitAdvanced>, <ore:plateIron>]
]);

// Satellite Bay
recipes.addShaped(satellite_bay, [
    [null, <ore:stickTitanium>, null],
    [<ore:stickTitanium>, basic_machine_core, <ore:stickTitanium>],
    [null, <ore:stickTitanium>, null]
]);

// Guidance Computer Access Hatch
recipes.addShaped(guidance_computer_access_hatch, [
    [null, <ore:circuitBasic>, null],
    [<ore:stickCopper>, basic_machine_core, <ore:stickCopper>],
    [null, <ore:circuitBasic>, null]
]);

// Observatory
recipes.addShaped(observatory, [
    [<ore:lensGlass>, <ore:plateGlass>, <ore:lensGlass>],
    [<ore:plateIron>, basic_machine_core, <ore:plateIron>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
]);

// Guidance Computer
recipes.addShaped(guidance_computer, [
    [<ore:circuitGood>, mv_sensor, <ore:circuitGood>],
    [<ore:plateIron>, basic_machine_core, <ore:plateIron>],
    [<ore:circuitGood>, <ore:plateIron>, <ore:circuitGood>]
]);

// Astrobody Data Analyzer
recipes.addShaped(astrobody_data_analyser, [
    [<ore:circuitGood>, <ore:plateGlass>, <ore:circuitGood>],
    [<ore:plateIron>, basic_machine_core, <ore:plateIron>],
    [<ore:circuitBasic>, <ore:plateIron>, <ore:circuitBasic>]
]);

// Satellite Terminal
recipes.addShaped(satellite_terminal, [
    [mv_sensor, <ore:plateGlass>, mv_sensor],
    [<ore:wireGtQuadrupleCopper>, basic_machine_core, <ore:wireGtQuadrupleCopper>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
]);

// Space Station Builder
recipes.addShaped(space_station_builder, [
    [null, null, null],
    [<ore:circuitAdvanced>, <advancedrocketry:rocketbuilder>, <ore:circuitAdvanced>],
    [null, null, null]
]);

// CO2 Scrubber
recipes.addShaped(<advancedrocketry:oxygenscrubber>, [
    [<ore:plateIron>, mv_pump, <ore:plateIron>],
    [<minecraft:iron_bars>, <ore:ingotCarbon>, <minecraft:iron_bars>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
]);

// Gas Charging Pad
recipes.addShaped(<advancedrocketry:oxygencharger>, [
    [null, <minecraft:heavy_weighted_pressure_plate>, null],
    [<ore:plateSteel>, basic_machine_core, <ore:plateSteel>],
    [mv_pump, <ore:plateSteel>, mv_pump]
]);

// Landing Pad
recipes.addShapeless(<advancedrocketry:landingpad>, [<ore:concrete>,plastic_plate]);

// Oxygen Vent
recipes.addShaped(<advancedrocketry:oxygenvent>, [
    [<ore:plateIron>, mv_pump, <ore:plateIron>],
    [<minecraft:iron_bars>, null, <minecraft:iron_bars>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
]);

// Warp Core
recipes.addShaped(<advancedrocketry:warpcore>, [
    [<ore:plateTitanium>, <ore:circuitAdvanced>, <ore:plateTitanium>],
    [<ore:plateSteel>, <ore:wireGtHexCopper>, <ore:plateSteel>],
    [<ore:plateTitanium>, <ore:circuitAdvanced>, <ore:plateTitanium>]
]);

// Warp Controller
recipes.addShaped(<advancedrocketry:warpmonitor>, [
    [<ore:plateSteel>, <ore:plateGlass>, <ore:plateSteel>],
    [<gregtech:meta_item_2:32448>, basic_machine_core, <gregtech:meta_item_2:32448>],
    [<ore:plateSteel>, <ore:circuitAdvanced>, <ore:plateSteel>]
]);

// Orientation Controller
recipes.addShaped(<advancedrocketry:orientationcontroller>, [
    [null, mv_sensor, null],
    [<gregtech:meta_item_2:32448>, basic_machine_core, <gregtech:meta_item_2:32448>],
    [null, <ore:circuitGood>, null]
]);

// Atmosphere Detector
recipes.addShaped(<advancedrocketry:oxygendetection>, [
    [<ore:plateSteel>, <ore:plateGlass>, <ore:plateSteel>],
    [<minecraft:iron_bars>, mv_pump, <minecraft:iron_bars>],
    [<ore:plateSteel>, mv_sensor, <ore:plateSteel>]
]);

// Atmosphere Terraformer
recipes.addShaped(<advancedrocketry:terraformer>, [
    [<ore:plateTitanium>, <ore:gemDilithium>, <ore:plateTitanium>],
    [<ore:circuitAdvanced>, advanced_machine_core, <ore:circuitAdvanced>],
    [<ore:plateTitanium>, <gregtech:meta_item_2:32448>, <ore:plateTitanium>]
]);

// Biome Scanner
recipes.addShaped(<advancedrocketry:biomescanner>, [
    [<ore:plateIron>, <advancedrocketry:satelliteprimaryfunction:5>, <ore:plateIron>],
    [mv_sensor, basic_machine_core, mv_sensor],
    [<ore:plateIron>, <ore:circuitGood>, <ore:plateIron>]
]);

// Suit Workshation
recipes.addShaped(<advancedrocketry:suitworkstation>, [
    [null, <ore:workbench>, null],
    [null, basic_machine_core, null],
    [null, <ore:circuitBasic>, null]
]);

// Solar Panel
recipes.addShaped(<advancedrocketry:solarpanel>, [
    [null, <gregtech:meta_item_1:32750>, null],
    [null, basic_machine_core, null],
    [null, <ore:wireGtQuadrupleCopper>, null]
]);

// Microwave Reciever
recipes.addShaped(<advancedrocketry:microwavereciever>, [
    [<gregtech:meta_item_1:32692>, <gregtech:meta_item_1:32692>, <gregtech:meta_item_1:32692>],
    [<ore:plateSteel>, <ore:wireGtHexCopper>, <ore:plateSteel>],
    [<ore:circuitAdvanced>, <ore:plateSteel>, <ore:circuitAdvanced>]
]);

// Drill
recipes.addShaped(<advancedrocketry:drill>, [
    [null, mv_pump, null],
    [null, basic_machine_core, null],
    [null, <ore:toolHeadDrillTitanium>, null]
]);

// Gravity Controller
recipes.addShaped(<advancedrocketry:gravitycontroller>, [
    [null, mv_emitter, null],
    [<gregtech:meta_item_2:32448>, basic_machine_core, <gregtech:meta_item_2:32448>],
    [null, <ore:circuitGood>, null]
]);

// Space Station Builder
recipes.addShaped(<advancedrocketry:deployablerocketbuilder>, [
    [null, null, null],
    [<ore:circuitGood>, <advancedrocketry:rocketbuilder>, <ore:circuitGood>],
    [null, null, null]
]);


//
// Ender IO Stuff
//

// Z Logic Unit recipe
var z_logic_unit = <enderio:item_material:41>;
recipes.addShapeless(z_logic_unit, [circuit_basic, <minecraft:rotten_flesh>]);
mods.jei.JEI.addItem(z_logic_unit);

// Skeletal Contractor recipe
var skeletal_contractor = <enderio:item_material:45>;
recipes.addShapeless(skeletal_contractor, [circuit_basic, <minecraft:bone>]);
mods.jei.JEI.addItem(skeletal_contractor);

// Remove power banks
mods.jei.JEI.removeAndHide(<enderio:block_cap_bank:0>);
mods.jei.JEI.removeAndHide(<enderio:block_cap_bank:1>);
mods.jei.JEI.removeAndHide(<enderio:block_cap_bank:2>);
mods.jei.JEI.removeAndHide(<enderio:block_cap_bank:3>);

// Remove power conduits
mods.jei.JEI.removeAndHide(<enderio:item_power_conduit:0>);
mods.jei.JEI.removeAndHide(<enderio:item_power_conduit:1>);
mods.jei.JEI.removeAndHide(<enderio:item_power_conduit:2>);

// Remove power guages
mods.jei.JEI.removeAndHide(<enderio:block_gauge>);
mods.jei.JEI.removeAndHide(<enderio:block_power_monitor>);
mods.jei.JEI.removeAndHide(<enderio:block_advanced_power_monitor>);

// Quite Clear Glass
alloy_smelter.recipeBuilder()
    .inputs(<ore:sand>, <ore:blockGlass>)
    .outputs(<enderio:block_fused_glass>)
    .duration(100)
    .EUt(8)
    .buildAndRegister();
mods.jei.JEI.addItem(<enderio:block_fused_glass>);

// Fused Quartz
alloy_smelter.recipeBuilder()
    .inputs(<minecraft:sand>, <ore:dustQuartzite>)
    .outputs(<enderio:block_fused_quartz>)
    .duration(160)
    .EUt(10)
    .buildAndRegister();
mods.jei.JEI.addItem(<enderio:block_fused_quartz>);
