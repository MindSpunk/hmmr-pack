
// Remove Crystallizer recipes
mods.advancedrocketry.Crystallizer.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:crystallizer>);

// Remove Rolling Machine recipes
mods.advancedrocketry.RollingMachine.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:rollingmachine>);

// Remove Plate Presser recipes
mods.advancedrocketry.PlatePresser.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:platepress>);

// Remove Centrifuge recipes
// mods.advancedrocketry.Centrifuge.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:centrifuge>);

// Remove Cutting Machine recipes
mods.advancedrocketry.CuttingMachine.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:cuttingmachine>);

// Remove Chemical Reactor recipes
mods.advancedrocketry.ChemicalReactor.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:chemicalreactor>);

// Remove Electrolyser recipes
mods.advancedrocketry.Electrolyser.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:electrolyser>);

// Remove Arc Furnace recipes
mods.advancedrocketry.ArcFurnace.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:arcfurnace>);

// Remove Precision Assembler recipes
mods.advancedrocketry.PrecisionAssembler.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:precisionassemblingmachine>);

// Remove Lathe recipes
mods.advancedrocketry.Lathe.clear();
mods.jei.JEI.removeAndHide(<advancedrocketry:lathe>);

// Remove Coal Generator
mods.jei.JEI.removeAndHide(<libvulpes:coalgenerator>);

// Remove Hovercraft
mods.jei.JEI.removeAndHide(<advancedrocketry:hovercraft>);

// Remove Jackhammer
mods.jei.JEI.removeAndHide(<advancedrocketry:jackhammer>);

